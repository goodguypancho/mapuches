//
//  main.m
//  Mapuches
//
//  Created by Francisco Gutiérrez on 1/11/12.
//  Copyright Universidad Técnica Federico Santa María 2012. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
    [pool release];
    return retVal;
}
