//
//  AppDelegate.h
//  Mapuches
//
//  Created by Francisco Gutiérrez on 1/11/12.
//  Copyright Universidad Técnica Federico Santa María 2012. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RootViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
	UIWindow			*window;
	RootViewController	*viewController;
}

@property (nonatomic, retain) UIWindow *window;

@end
