//
//  Mapuche.h
//  Mapuches
//
//  Created by Francisco Gutiérrez on 1/11/12.
//  Copyright (c) 2012 Universidad Técnica Federico Santa María. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Mapuche : NSObject{
    int maxHealth;
    int currentHealth;
}

-(void) printCurrentHealth;
-(void) printMaxHealth;

@end


