//
//  Mapuche.m
//  Mapuches
//
//  Created by Francisco Gutiérrez on 1/11/12.
//  Copyright (c) 2012 Universidad Técnica Federico Santa María. All rights reserved.
//

#import "Mapuche.h"

@implementation Mapuche

-(id)init{
    if ((self=[super init])) {
        printf("Initializin Mapuche\n");
        maxHealth = 100;
        currentHealth = 100;
    }
    return self;
}

-(void) printMaxHealth{
    printf("Mapuche Max Health: %d\n",maxHealth);
}

@end
