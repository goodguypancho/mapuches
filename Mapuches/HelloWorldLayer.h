//
//  HelloWorldLayer.h
//  Mapuches
//
//  Created by Francisco Gutiérrez on 1/11/12.
//  Copyright Universidad Técnica Federico Santa María 2012. All rights reserved.
//


// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Splash.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer
{
    Splash *splash;
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
